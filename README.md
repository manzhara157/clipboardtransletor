# Clipboard Translator

### What does this program do?
   Pytthon script read information from clipboard and translates the content. To translation use google translator.
 
### This is python script. To run it you need:
#### Install dependencies:
   - python - obviously
   - plyer - to notification
   - clipboard - to monitoring clipboard
   - googletranslatepy - to use google api

#### To install python do

  On Debian/Ubuntu
> **sudo apt install python3**

  <!-- On Manjaro Linux -->
 <!-- >**sudo pacman -S jdk11-openjdk** -->
<!-- > -->
 <!-- >**sudo pacman -S maven git** -->

#### To install this plugins do
> pip install plyer

> pip install clipboard

> python3 -m pip install googletranslate-python

#### Clone project
I recommend put folder with file to /opt/, but you may put script in any place.

> **cd /opt**
>
> **sudo git clone https://gitlab.com/manzhara157/clipboardtransletor.git**

#### Run python file
> **python /path/to/script/clTranslator.py**

#### Create hotkey

The method depends on your work environment. So I can't give any recommendations.


#### Note

If you heve instaletion error related to the virtual environment delete file `EXTERNALLY-MANAGED` by command:
```
# rm /usr/lib/python3.11/EXTERNALLY-MANAGED
```
