#!/bin/python3

import clipboard
import time
import sys
import os

from googletranslatepy import Translator

sys.path.append(os.path.abspath("SO_site-packages"))

import pyperclip

from plyer import notification

translator = Translator(target='uk')


notification.notify(
    title = 'Clipboard Translator',
    message = 'Перекладаю...',
    # app_icon = None,
    timeout = 3,
)

def ask(questions):
    answer = translator.translate(questions)
    return answer

recent_value = ""
tmp_value = pyperclip.paste()
if tmp_value != recent_value:
    recent_value = tmp_value
    trnsl_value = ask(recent_value)
    print("Value changed: %s" % str(recent_value)[:30])
    print("Translation:")
    print(trnsl_value)
    notification.notify(
        title = 'Переклад',
        message = trnsl_value,
        timeout = 15,
    )
    time.sleep(0.1)

