#!/usr/bin/env python3

import os
import sys

from googletranslatepy import Translator

langList = [ 'en', 'pl', 'ru' ]

# translator = Translator(target='en')
# translator = Translator(target='pl')
# translator = Translator(target='ru')

def ask(questions, tr):
    answer = tr.translate(questions)
    return answer

i = 0
line_number = 0
for lng in langList:
    nameOfFile = "translation_" + lng + ".sbv"
    f = open(nameOfFile, "w")
    f.close()

with open("captions.sbv", "r") as f_in:
    lines = (line.rstrip() for line in f_in) 
    lines = list(line for line in lines if line) # From File
    line_number = sum(1 for _ in lines)

for lng in langList:
    nameOfFile = "translation_" + lng + ".sbv"
    t = open(nameOfFile, "a") # To File
    translator = Translator(target=lng)
    i = 0
    print("Start translate to " + lng)
    for x in lines:
        i = i + 1
        print("Translate part: " + str(i),"/",str(line_number))
        if x[0].isdigit():
            # t.write(x)
            # t.write(f" \n")
            print(f"", file=t)
            print(x, file=t)
        else:
            if x.strip():
                ans = ask(x, translator)
                # t.write(f"{ans} \n")
                print(f"{ans} ", file=t)

    t.close()


print("Done!")
    
