import os
import sys
# import discord
# from discord.ext import commands
# from discord_components import *
# import asyncio
import openai

openai.api_key = "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"
model_engine = "text-davinci-003"
# text0 = "Перекладіть, будь ласка, цей текст з української на польську, звертаючи особливу увагу на його смисл та контекст, щоб забезпечити максимальну точність та зрозумілість перекладу. Дотримуйтесь вільного перекладу, якщо дослівний переклад може спотворити зміст або сенс тексту: "
# text1 = "Перекладіть, будь ласка, цей текст з української на англійську, звертаючи особливу увагу на його смисл та контекст, щоб забезпечити максимальну точність та зрозумілість перекладу. Дотримуйтесь вільного перекладу, якщо дослівний переклад може спотворити зміст або сенс тексту. "
# text2 = "Перекладіть, будь ласка, цей текст з української на російську, звертаючи особливу увагу на його смисл та контекст, щоб забезпечити максимальну точність та зрозумілість перекладу. Дотримуйтесь вільного перекладу, якщо дослівний переклад може спотворити зміст або сенс тексту: "
text3 = "Веди себе як професійний перекладач на різні мови. Намагайся перекладати якомога змістовніше."
# langList = [ "російську", "англійську", "польску" ]
langList = [ "російську", "польську", "англійську" ]

def ask(questions, langTo, chat_log=None):
    # if chat_log is None:
    #     chat_log = messages_mas
    # print(questions)
    prompt = f'{text3}Human: Переклади \"{questions}\" на {langTo}.\nAI:' 
    response = openai.Completion.create(
        engine=model_engine,
        stop=['\nHuman'],
        prompt=prompt,
        top_p=1,
        temperature=0.9,
        max_tokens=1024,
        # n=1,
        frequency_penalty=0,
        presence_penalty=0.6,
        best_of=1
        )
    answer = response.choices[0].text.strip()
    return answer

i = 0
line_number = 0
f = open("translation", "w")
# f.write("Translation: \n")
f.close()

with open("captions.sbv", "r") as f_in:
    lines = (line.rstrip() for line in f_in) 
    lines = list(line for line in lines if line) # From File
    line_number = sum(1 for _ in lines)

t = open("translation", "a") # To File
for lng in langList:
    # t.write(lng)
    for x in lines:
        i = i + 1
        print("Translate part: " + str(i),"/",str(line_number))
        if x[0].isdigit():
            t.write(x)
            t.write(f" \n")
        else:
            if x.strip():
                ans = ask(x, lng)
                t.write(f"{ans} \n")
            # else:
            #     print('The line is empty')


print("Done!")
f.close()
t.close()
    
